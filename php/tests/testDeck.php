<?php

use PHPUnit\Framework\TestCase;

include 'src/app.php';

class testDeck extends TestCase
{
    /**
     * Calculate the probability of getting a draw of the same card suit
     *
     * @param int $recurrence
     * @return float|int
     */
    private function probabilityOfSuitedCards(int $recurrence)
    {
        $rank = count(RANKS);
        $deck = TOTAL_CARDS;

        $result = 1;
        for ($i = 0; $i < $recurrence; ++$i) {
            $result = $result * ($rank-$i)/($deck-$i);
        }
        return $result;
    }

    /**
     * Test the desk construction
     */
    public function test_deck_construction()
    {
        $deck = new Deck(SUITS, RANKS);
        $this->assertCount(TOTAL_CARDS, $deck->getAllCards());
    }

    /**
     * Test the desk suffle
     */
    public function test_deck_shuffle()
    {
        /**
         * Maximum continuous suited cards before considering the deck isn't
         * well shuffled.
         * The probability of getting a draw of the same card suit 8 times is
         * ~1.71 over 1.000.000.
         */
        $maxContinuousSuit = 8;

        /**
         * Number of shuffle test to run: 1.000.000 is quite enough
         */
        $runTestNumber = 1000000;

        /**
         * Maximum shuffle tests failures before consider it's not only bad luck.
         * The limit of 2 failures is too risky, 3 for 1 billion of tests is
         * satisfacting enough.
         */
        $maxNotWellShuffled = 3;

        /**
         * "Not well shuffled" deck counter.
         */
        $notWellShuffled = 0;

        for ($i = 0; $i < $runTestNumber; ++$i) {
            /**
             * Continuous suit cards counter
             */
            $continuousSuit = 0;

            $deck = new Deck(SUITS, RANKS);
            $deck->shuffle();

            for ($y = 1; $y <= $maxContinuousSuit; ++$y) {

                $last = $deck->getAllCards()[($y-1)];
                $card = $deck->getAllCards()[$y];

                if ($card->getsuit() === $last->getSuit()) {
                    // Same suit card with the last one, increase the counter.
                    ++$continuousSuit;
                    // We reach the limit of max continuous suited cards,
                    // the deck is not well shuffled, increase the counter.
                    if ($continuousSuit === $maxContinuousSuit) {
                        ++$notWellShuffled;
                        break;
                    }
                } else {
                    // Well done shuffle function !
                    break;
                }
            }
            /**
             * Test if we aren't above the max of "not well shuffled" count
             */
            $this->assertLessThan($maxNotWellShuffled, $notWellShuffled);
        }
    }
}
