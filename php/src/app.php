<?php

const SUITS = ['SPADES', 'HEARTS', 'DIAMONDS', 'CLUBS'];
const RANKS = ['ACE', 'TWO', 'TREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'HEIGHT', 'NINE', 'TEN', 'JACK', 'QUEEN', 'KING'];
define('TOTAL_CARDS', count(SUITS) * count(RANKS));

/**
 * Class card
 */
class card
{
    private $suit = '';
    private $rank = '';

    public function __construct(string $suit, string $rank)
    {
        $this->suit = $suit;
        $this->rank = $rank;
    }

    /**
     * @return string
     */
    public function getSuit()
    {
        return $this->suit;
    }

    /**
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }
}

/**
 * Class deck
 */
class deck
{
    private $deck = [];

    public function __construct(array $suits, array $ranks)
    {
        foreach ($suits as $suit) {
            foreach ($ranks as $rank) {
                $this->deck[] = new card($suit, $rank);
            }
        }
    }

    /**
     * @return array
     */
    public function getAllCards()
    {
        return $this->deck;
    }

    /**
     * @return boolean
     */
    public function shuffle()
    {
        return shuffle($this->deck);
    }
}

