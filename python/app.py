#!/usr/bin/python3
# coding: utf-8

from random import shuffle
import numpy

SUITS = ('SPADES', 'HEARTS', 'DIAMONDS', 'CLUBS')
RANKS = ('ACE', 'TWO', 'TREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'HEIGHT', 'NINE', 'TEN', 'JACK', 'QUEEN', 'KING')
TOTAL_CARDS=len(SUITS) * len(RANKS)

class Card(object):

    suit = ''
    rank = ''

    def __init__(self, suit: str, rank: str):
        self.suit = suit
        self.rank = rank

class Deck(object):

    deck = []

    def __init__(self, suits, ranks):
        for suit in suits :
            for rank in ranks :
                self.deck.append(Card(suit, rank))

    def shuffle(self):
        shuffle(self.deck)
