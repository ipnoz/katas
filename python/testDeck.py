#!/usr/bin/python3
# coding: utf-8

import app
import unittest

class testDeck(unittest.TestCase):

    # Test the desk construction
    def test_deck_construction(self):
        deck = app.Deck(app.SUITS, app.RANKS)
        self.assertEqual(app.TOTAL_CARDS, len(deck.deck))

    # Test the desk suffle
    def test_deck_shuffle(self):

        # Maximum continuous suited cards before considering the deck isn't
        # well shuffled.
        # The probability of getting a draw of the same card suit 8 times is
        # ~1.71 over 1.000.000.
        maxContinuousSuit = 8

        # Number of shuffle test to run: 1.000.000 is quite enough
        runTestNumber = 1000000
        # python is too slow for running to much shuffle tests, and isn't
        # designed for it as my RAM and swap have been full filled (10Go)
        runTestNumber = 1000

        # Maximum shuffle tests failures before consider it's not only bad luck.
        # The limit of 2 failures is too risky, 3 for 1 billion of tests is
        # satisfacting enough.
        maxNotWellShuffled = 3
        # Without 1 billion of tests, 2 is really enough
        maxNotWellShuffled = 2

        # Not well shuffled" deck counter.
        notWellShuffled = 0;

        for i in range(runTestNumber) :

            # Uncomment to see how slow the tests are...
            # print('runTestNumber: %d' % i)

            # Continuous suit cards counter
            continuousSuit = 0

            deck = app.Deck(app.SUITS, app.RANKS)
            deck.shuffle()

            for y in range(maxContinuousSuit) :

                if y == 0 :
                    continue

                last = deck.deck[(y-1)];
                card = deck.deck[y];

                if card.suit == last.suit :
                    # Same suit card with the last one, increase the counter.
                    continuousSuit += 1
                    # We reach the limit of max continuous suited cards,
                    # the deck is not well shuffled, increase the counter.
                    if continuousSuit == maxContinuousSuit :
                        notWellShuffled += 1
                        break
                else :
                    # Well done shuffle function !
                    break
            #Test if we aren't above the max of "not well shuffled" count
            self.assertGreater(maxNotWellShuffled, notWellShuffled)


if __name__ == '__main__':
    unittest.main()
